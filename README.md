Uses Grunt task runner to compile SASS and concat files.
## Build
```
git clone https://bitbucket.org/amp1ee/popup-task.git
cd popup-task
grunt all
```
## Launch
To automatically open the `popup.html` page execute this command (requires Google Chrome on Linux).
```
grunt run
```