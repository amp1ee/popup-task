function popupClose() {
	$('.popup')
	.animate({opacity: 0}, 300,
		function(){
			$(this).css('display', 'none');
			$('.popup-underlay').fadeOut(400);
		}
		);
}

$(document).ready(function() {
	$('.popup-button_show').click(function(event) {
		$('.popup-underlay').fadeIn(300, 
			function() {
				$('.popup').css('display', 'block')
				.animate({
					opacity: 1
				}, 300);
			});
	});

	$('.popup-button_close, .popup-underlay').click(function() {
		popupClose();
	});

	$('.popup-footer__button_uninstall').click(function() {
		alert("DONE");
		popupClose();
	});
});