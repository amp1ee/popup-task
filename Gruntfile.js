module.exports = function(grunt) {

	// Config
	var incl_dir = 'common.blocks/';
	var build_dir = 'build/';

	const sass = require('node-sass');

	grunt.initConfig({
		concat: {
			js: {
				src: [incl_dir + '*.js'],
				dest: build_dir + 'scripts.js'
			},
			css: {
				src: [incl_dir + '*.css'],
				dest: build_dir + 'style.css'
			}
		},
		sass: {
			options: {
				implementation: sass,
				sourceMap: true
			},
			dist: {
				files: {
					'common.blocks/popup.css' : 'common.blocks/sass/popup.scss'
				}
			}
		},
		open : {
			chrome: {
				path: 'popup.html',
				app: 'google-chrome'
			}
		}
	});

	// Load plugins
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-open');

	// Register tasks
	grunt.registerTask('all', ['sass', 'concat']);
	grunt.registerTask('run', 'open:chrome');
	grunt.registerTask('concat-js', ['concat:js']);
	grunt.registerTask('concat-css', ['concat:css']);
}
